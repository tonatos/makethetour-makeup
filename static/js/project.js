var markerClickCallback = function() {
    // Показываем popup
    $.fancybox({
        href : '/popup_item',
        title : 'Кинотеатр Prada 3D',
        type: 'ajax',
        width: '100%',
        height: 'auto',
        maxWidth: '600px',
        autoSize: false,
        fitToView: false,
        closeBtn: false,
        helpers : {
            title : null
        },
        tpl: {
            'wrap': '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div><a href="#" class="btn-close">Закрыть</a></div></div>'
        }
    });
}
function initialize() {
  var mapOptions = {
    zoom: 16,
    center: new google.maps.LatLng(56.837846,60.603225),
    width: '100%',
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'),
                                mapOptions);

  var image = {
    url: '../static/img/icon-cafe.png',
    size: new google.maps.Size(20, 20),
    origin: new google.maps.Point(0,0),
    anchor: new google.maps.Point(10, 30)
  };

  var marker1 = new google.maps.Marker({
        position: new google.maps.LatLng(56.837846,60.603225),
        map: map,
        shadow: '../static/img/marker-orange.png',
        icon: image,
        shape: {
          coord: [1, 1, 1, 20, 18, 20, 18 , 1],
          type: 'poly'
        },
        title: 'Екатеринбург',
  });
  var marker2 = new google.maps.Marker({
        position: new google.maps.LatLng(56.8377060000, 60.6043150000),
        map: map,
        shadow: '../static/img/marker-green.png',
        icon: image,
        shape: {
          coord: [1, 1, 1, 20, 18, 20, 18 , 1],
          type: 'poly'
        },
        title: 'Екатеринбург'
  });
  google.maps.event.addListener(marker1, 'click', markerClickCallback);
  google.maps.event.addListener(marker2, 'click', markerClickCallback);

  map_arr = new Array();

  for (i=0; i<15; i++) {
    map_arr[i] = new google.maps.Marker({
        position: new google.maps.LatLng(
            56.837000 + Math.random() / 1000,
            60.600000 + Math.random() / 100),
        map: map,
        shape: {
          coord: [1, 1, 1, 20, 18, 20, 18 , 1],
          type: 'poly'
        },
        icon: image = {
            url: '../static/img/point-orange.png',
            size: new google.maps.Size(20, 20),
            origin: new google.maps.Point(0,0),
            anchor: new google.maps.Point(10, 30)
        },
        title: 'Екатеринбург'
    });
    google.maps.event.addListener(map_arr[i], 'click', markerClickCallback);
  }
}
$(document).ready(function(){
    $('.fancybox').fancybox({
        type: 'ajax',
        width: '100%',
        height: 'auto',
        autoSize: false,
        fitToView: false,
        closeBtn: false,
        maxWidth: 800,
        helpers : {
            title : null
        },
        tpl: {
            'wrap': '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div><a href="#" class="btn-close">Закрыть</a></div></div>'
        },
        afterShow: function(){
            $('.chosen').chosen({
                'search_contains': true,
                'is_multiple': false,
                'disable_search_threshold': true,
            });
        }
    });

    // Дропдауны
    $('.dropdown-toggle').dropdown();

    // Закрываем его
    $('.btn-close').live('click', function(){
        $.fancybox.close();
    });

    /**
     * Тут мы пилим категории.
     */
    $('.categories ul li').each(function(i, obj){
        var active = $(obj).hasClass('active');
        $(obj).prepend(
            '<input type="checkbox" ' +
                'id="id_' + $(this).data('name') + '" ' +
                'name="' + $(this).data('name') + '" ' +
                'checked="' + active + '" ' +
                'value="' + $(this).data('value') + '" ' +
                'style="display: none" />'
        );
    });
    $('.categories ul li span a').live('click', function(){
        var parent = $(this)
                        .parent()
                        .parent();

        if ( parent.hasClass('active') ) {
            parent.removeClass('active');
            parent.find('input').attr('checked', false);
        } else {
            parent.addClass('active');
            parent.find('input').attr('checked', true);
        }
    });
    $('#has-complite').ready(function(){
        var checkState = function(){
            var state = $(this).attr('checked');
            if ( !state ) {
                $('#has-complite')
                    .addClass('disabled')
                    .find('input')
                    .attr('disabled', true);
            } else {
                $('#has-complite')
                    .removeClass('disabled')
                    .find('input')
                    .attr('disabled', false);

            }
        }

        $('#checkbox-has-complite').change(checkState)
        checkState();
    })

    $('.chosen').chosen({
        'search_contains': true,
        'is_multiple': false,
        'disable_search_threshold': true,
    });
})
