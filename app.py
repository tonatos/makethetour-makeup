#!/usr/bin/env python
#-*- coding: UTF-8 -*-

import os
import re

from flask import Flask
from flask import render_template
from flask import make_response
from functools import update_wrapper
from jinja2.exceptions import TemplateNotFound


app = Flask(__name__)
#app.jinja_env.add_extension('jinja2htmlcompress.SelectiveHTMLCompress')

PATH = os.path.dirname(__file__)
TEMPLATE_DIR = 'templates'
STATIC_DIR = 'static'
EXPORT_DIR = 'export'

def nocache(f):
    """
    Отменяем кеш
    """

    def new_func(*args, **kwargs):
        resp = make_response(f(*args, **kwargs))
        resp.cache_control.no_cache = True
        return resp
    return update_wrapper(new_func, f)

class Node():
    """
    Класс элемента списка файлов и директорий. Маленький простенький "нодик".
    """

    isdir = False
    name = ''
    path = ''
    files = []

    def __init__(self, isdir = True, name = '', path = '', files = []):
        self.isdir = isdir
        self.name = name
        self.path = path.replace( os.path.join(PATH, TEMPLATE_DIR) , '')\
                        .replace('.html', '')\
                        .replace('\\', '/')

        self.files = files

def __get_files( dir ):
    """
    Хелпер для рекурсивного пробега по директории с темплейтами. На случай,
    если у нас появляются уровни вложенности
    """

    files = []
    for f in os.listdir( dir ):
        # Если это директория или html файлик, то работаем дальше
        if (os.path.isdir( os.path.join(dir, f))
                or '.html' in f) \
            and not f.startswith('__'):

            # Если это папочка, то вот она - рекурсия!
            if os.path.isdir(os.path.join(dir,f)):
                files.append( Node(True, f,
                                    os.path.join(dir, f),
                                    __get_files( os.path.join(dir,f))) )
                continue

            files.append( Node(False, f, os.path.join(dir, f)) )

    return files


@nocache
@app.route('/')
@app.route('/<page>/')
@app.route('/<dir>/<page>/')
def any_page(dir='/', page='/'):
    """
    Собственно, роут для шаблонов. Открывает наши файлики
    """

    if page == '/':
        return render_template('__list.html',
            node=Node(True, TEMPLATE_DIR, '/',
                __get_files( os.path.join(PATH, TEMPLATE_DIR))))
    try:
        return render_template( os.path.join(dir, page + '.html').replace('\\', '/') )
    except TemplateNotFound:
        return render_template('__404.html')


@app.route('/__export/')
def export(page='/'):
    """
    Роут для экспорта всех темплейтов в самые тупы и обычные html файлы.
    Для всяких программистов, которые не могут осилить темплейты
    """

    result = ''
    export_dir = os.path.join(PATH, EXPORT_DIR)

    def __export_helper(files, export_dir):
        result = ''
        for f in files:
            # Проверяем, есть ли такая директория
            if not os.path.exists( export_dir ):
                os.mkdir( export_dir )

            # Если это внезапно папка - то рекурсивно пробегаемся по ней
            if f.isdir:
                result += __export_helper(f.files, os.path.join(export_dir, f.name))
                continue

            export_template = render_template( f.path + '.html' )

            # Пытаемся экспортировать
            try:
                export_template_link = open(
                    os.path.join(export_dir, f.name), mode='wb')

                # Делаем относительный путь, чтобы не от корня считался, а от файлика
                relative_path = os.path.relpath(
                                    os.path.join(PATH, STATIC_DIR),
                                    os.path.join(PATH, export_dir) )\
                                    .replace('\\', '/')

                # Нагло реплейсим пути к статике прямо внутри шаблона
                export_template = export_template.replace('/%s' % STATIC_DIR, relative_path)

                export_template_link.write( export_template.encode('utf-8') )
                export_template_link.close()

                # Пишем в лог, что все ок.
                result += 'export templates %s\n<br>' % f.path
            except IOError:
                # Если нет прав на запись или не можем найти папочку — эксептим
                result += '[!] export templates error: %s\n<br>' % f.path

        return result

    result += __export_helper(
        __get_files(os.path.join(PATH, TEMPLATE_DIR)),
        export_dir)

    return result

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
